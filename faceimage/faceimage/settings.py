# Scrapy settings for faceimage project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'faceimage'
BOT_VERSION = '1.0'

SPIDER_MODULES = ['faceimage.spiders']
NEWSPIDER_MODULE = 'faceimage.spiders'
DEFAULT_ITEM_CLASS = 'faceimage.items.FaceimageItem'
USER_AGENT = '%s/%s' % (BOT_NAME, BOT_VERSION)

