from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from scrapy.http.request import Request
from faceimage.items import FaceimageItem

class ScrapyOrgSpider(BaseSpider):
    name = "fc"
    allowed_domains = ["www.nykaa.com"]
    start_urls = ["http://www.nykaa.com/fragrance/men.html"]

    def parse(self, response):
        hxs = HtmlXPathSelector(response)
        images = hxs.select("//div[@class='homeProducts']/ul/li")
        items = []
        for image in images:
            item = FaceimageItem()
            item["img"] = image.select("a[@class='product-image']/img/@src").extract()
            item["productname"] = image.select("div[@class='catgery_Products_title']/h1/a/text()").extract()
            item["link"] = image.select("div[@class='catgery_Products_title']/h1/a").extract()
            item["desc"] = image.select("div[@class='catgery_product_box']/text()").extract()
            items.append(item)
        for item in items:
            yield item
