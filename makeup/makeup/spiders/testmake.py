from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from makeup.items import MakeupItem

class MySpider(BaseSpider):
    name = "ny"
    allowed_domains = ["www.nykaa.com"]
    start_urls = ["http://www.nykaa.com/"]

    def parse(self, response):
        hxs = HtmlXPathSelector(response)
	catmakeup=hxs.select('//ul/li')
        categories1 = []
	for mp in catmakeup:
	    mpk=MakeupItem()
	    mpk ["mkname"]=mp.select('//a[@class="level-top"]/span/text()').extract()
	    mpk ["mklink"]=mp.select('//a[@class="level-top"]/@href').extract()
	    categories1.append(mpk)
        return categories1
