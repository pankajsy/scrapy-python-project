from scrapy.contrib.spider import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector
from nyka_sample.items import NykaSampleItem

class MySpider(CrawlSpider):
    name = "nyk1"
    allowed_domains = ["craiglist.org"]
    start_urls = ["http://sfbay.craiglist.org/npo/"]

    rules = (Rule (SgmlLinkExtractor(allow=("index\d00\.html",
    ),restrict_xpaths=('//p[@id="nextpage"]',))
    , callbacck="parse_items", follow=True),
    )

    def parse_items(self, response):
        hxs = HtmlXPathSelector(response)
        titles = hxs.select("//p")
        items = []
        for titles in titles:
            item = NykaSampleItem()
            item ["title"] = titles.select("a/text()").extract()
            item ["link"] = titles.select("a/@href").extract()
            items.append(item)
        return items
