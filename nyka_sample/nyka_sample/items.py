from scrapy.item import Item, Field

class NykaSampleItem(Item):
    # define the fields for your item here like:
    # name = Field()
    title = Field()
    link = Field()
    
    pass
