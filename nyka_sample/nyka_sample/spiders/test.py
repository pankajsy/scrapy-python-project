from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from nyka_sample.items import NykaSampleItem

class MySpider(BaseSpider):
    name = "nyk123"
    allowed_domains = ["www.nykaa.com"]
    start_urls = ["http://www.nykaa.com/fragrance/men/sprays.html"]

    def parse(self, response):
        hxs = HtmlXPathSelector(response)
        titles = hxs.select("//ul/li")
	
        items = []
        for title in titles:
            item = NykaSampleItem()
            item ["title"] = title.select("a/span/text()").extract()
            item ["link"] = title.select("a/@href").extract()
           
            items.append(item)
        return items
