# Scrapy settings for nyka_sample project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'nyka_sample'
BOT_VERSION = '1.0'

SPIDER_MODULES = ['nyka_sample.spiders']
NEWSPIDER_MODULE = 'nyka_sample.spiders'
DEFAULT_ITEM_CLASS = 'nyka_sample.items.NykaSampleItem'
USER_AGENT = '%s/%s' % (BOT_NAME, BOT_VERSION)

